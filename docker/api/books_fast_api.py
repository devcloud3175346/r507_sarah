import datetime
import os
import pathlib
import sqlite3
import uvicorn
from datetime import datetime
from fastapi import FastAPI, Path, Query, Request, HTTPException, Response
from typing import Annotated, Optional, Union
from pydantic import BaseModel
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt

app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
api_service_auth = os.environ.get("API_SERVICE_AUTH_URL", "http://auth:5005")
URL_EXO = api_service_auth + "/"

SECRET_KEY = "secret_key_507"
ALGORITHM = "HS256"

def convert_result(rows, fields):
    result = [dict(zip(fields, row)) for row in rows]
    return result

def result_to_dict(result, fields):
    if result:
        return {field: result[0][i] for i, field in enumerate(fields)}
    return None

def show_table(bdd_name: str, table_name: str):
    with sqlite3.connect(bdd_name) as conn:
        cur = conn.cursor()
        rows =cur.execute(f"SELECT * FROM {table_name}").fetchall()
        if not rows:
            raise HTTPException(status_code=404, detail="Aucune donnée dans la table")
        fields = [column[0] for column in cur.description]
    return convert_result(rows, fields)

def show_user(bdd_name, username):
    with sqlite3.connect(bdd_name) as conn:
        cur = conn.cursor()
        try:
            id = int(username)
            rows = cur.execute("SELECT * FROM utilisateurs WHERE user_id = ?", (id,)).fetchall()
        except ValueError:
            rows = cur.execute("SELECT * FROM utilisateurs WHERE username LIKE ?", (f"{username}%",)).fetchall()
        if not rows:
            raise HTTPException(status_code=404, detail="Utilisateur non trouvé dans la base de données")
        fields = [column[0] for column in cur.description]
    return convert_result(rows, fields)

def show_book(bdd_name, title):
    with sqlite3.connect(bdd_name) as conn:
        cur = conn.cursor()  
        try:
            id = int(title)
            rows = cur.execute("SELECT * FROM livres WHERE id = ?", (id,)).fetchall()
        except ValueError:
            rows = cur.execute("SELECT * FROM livres WHERE titre LIKE ?", (f"%{title}%",)).fetchall()
        if not rows:
            raise HTTPException(status_code=404, detail="Livre non trouvé dans la base de données")
        fields = [column[0] for column in cur.description]
    return convert_result(rows, fields)

def show_user_books(bdd_name, username):
    with sqlite3.connect(bdd_name) as conn:
        cur = conn.cursor()
        user_id = cur.execute("SELECT user_id FROM utilisateurs WHERE username = ? OR user_id = ?", (username, username)).fetchone()[0]
        if not user_id:
            raise HTTPException(status_code=404, detail="Utilisateur non trouvé")
        rows = cur.execute("SELECT * FROM livres WHERE emprunteur_id = ?", (user_id, )).fetchall()
        if not rows:
            raise HTTPException(status_code=404, detail="L'utilisateur n'a pas emprunté de livre")
        fields = [column[0] for column in cur.description]
    return convert_result(rows, fields)

def show_book_siecle(bdd_name, siecle):
    with sqlite3.connect(bdd_name) as conn:
        cur = conn.cursor()
        rows = cur.execute("SELECT * FROM livres WHERE SUBSTR(date_public, 7, 2) = ?", (siecle,)).fetchall()
        if not rows:
            raise HTTPException(status_code=404, detail="Aucun livre de ce siècle n'a été trouvé")
        fields = [column[0] for column in cur.description]
    return convert_result(rows, fields)

def add_book_db(bdd_name, titre, pitch, auteur, date):
    with sqlite3.connect(bdd_name) as conn:
        cur = conn.cursor()
        if titre not in [row[0] for row in cur.execute("SELECT titre FROM livres").fetchall()]:
            if auteur not in [row[0] for row in cur.execute("SELECT nom_auteur FROM auteurs").fetchall()]:
                cur.execute("INSERT INTO auteurs (nom_auteur) VALUES (?)", (auteur,))
                print(f"Auteur {auteur} ajouté à la table")
            id_auteur = cur.execute("SELECT id FROM auteurs WHERE nom_auteur = ?", (auteur,)).fetchone()[0]
            cur.execute("INSERT INTO livres (titre, pitch, auteur_id, date_public) VALUES (?, ?, ?, ?)", (titre, pitch, id_auteur, date))
            return {"id": None, "titre": titre, "pitch": pitch, "auteur": auteur, "auteur_id": None, "date_public": date, "emprunteur_id": None}
        else :
            raise HTTPException(status_code=400, detail="Le livre est déjà renseigné dans la base de données")

def add_user_db(bdd_name, username, email):
    with sqlite3.connect(bdd_name) as conn:
        cur = conn.cursor()
        if username not in [row[0] for row in cur.execute("SELECT username FROM utilisateurs").fetchall()]:
            cur.execute("INSERT INTO utilisateurs (username, email) VALUES (?, ?)", (username, email))
            return {'CREATED':f"User {username} ajouté à la table avec le mail {email}"}, 201
        else :
            raise HTTPException(status_code=400, detail="L'utilisateur existe déjà dans la base de données")
        
def delete_user(bdd_name, utilisateur):
    with sqlite3.connect(bdd_name) as conn:
        cur = conn.cursor()
        cur.execute("DELETE FROM utilisateurs WHERE username = ?", (utilisateur,))
        return {'SUCCESS': f'Le user {utilisateur} a  été supprimé de la base de données'}, 200

class Utilisateur(BaseModel):
    user_id: int
    username: str
    email: str

class Livre(BaseModel):
    id: Optional[int]
    titre: str
    pitch: str
    auteur_id: Optional[int]
    date_public: str
    emprunteur_id: Optional[int]

class Auteur(BaseModel):
    id: int
    nom_auteur: str
    
database = pathlib.Path(__file__).parent.absolute() / "data" / "database.db"

@app.middleware("http")
async def verify_token(request: Request, call_next):
    #exclure les routes de la vérification du token
    if request.url.path == "/docs" or request.url.path == "/redoc" or request.url.path == "/openapi.json" or request.url.path == "/token":
        response = await call_next(request)
        return response
    token = request.headers.get('Authorization')
    if token:
        token = token.split("Bearer ")[-1]
        try:
            payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
            username: str = payload.get("sub")
            if username is None:
                return Response("Token has expired", status_code=403)
        except jwt.ExpiredSignatureError:
            return Response("Token has expired", status_code=403)
        except JWTError:
            return Response(content="Invalid token", status_code=400)
    else:
        return Response(content="Missing token", status_code=400)
    response = await call_next(request)
    return response    

@app.get('/utilisateurs', response_model=list[Utilisateur])
def get_users():
    """Get all users from database"""
    return show_table(database, "utilisateurs")

@app.get('/livres', response_model=list[Livre])
def get_livres():
    """Get all books from database"""
    return show_table(database, "livres")

@app.get('/auteurs', response_model=list[Auteur])
def get_auteurs():
    """Get all authors from database"""
    return show_table(database, "auteurs")

@app.get('/utilisateur/{utilisateur}', response_model=list[Utilisateur])
def get_user_by_name_or_id(utilisateur: Annotated[Union[int, str], Path(..., description="User name or id")]):
    """Get user by name or id"""
    return show_user(database, utilisateur)

@app.get('/utilisateur/emprunts/{utilisateur}', response_model=list[Livre])
def get_user_books(utilisateur: Annotated[Union[int, str], Path(..., description="User name or id")]):
    """Get user books by name or id"""
    return show_user_books(database, utilisateur)

@app.get('/livres/siecle/{siecle}', response_model=list[Livre])
def get_book_by_siecle(siecle: Annotated[str, Path(..., description="Book publication century (ex: 19)")]):
    """Get books by century"""
    return show_book_siecle(database, siecle)

@app.get('/livre/{livre}', response_model=list[Livre])
def get_book_by_name_or_id(livre: Annotated[Union[int, str], Path(..., description="Book name or id")]):
    """Get user by name or id"""
    print(livre)
    return show_book(database, livre)

@app.post('/livres/ajouter', status_code=201)
def add_book(request: Request,
             titre: str = Query(..., description="Book title", min_length=1, max_length=50),
             pitch: str = Query(..., description="Book pitch", min_length=1, max_length=500),
             auteur: str = Query(..., description="Book author", min_length=1, max_length=50),
             date: str = Query(..., description="Book publication date (ex: 01/01/2001)")):
    """Add a book to database"""
    return add_book_db(database, titre, pitch, auteur, date)

@app.post('/utilisateurs/ajouter', status_code=201)
def add_user(request: Request,
             username: str = Query(..., description="User name", min_length=1, max_length=50),
             email: str = Query(..., description="User email", min_length=1, max_length=50)):
    """Add a user to database"""
    return add_user_db(database, username, email)

@app.delete('/utilisateurs/{utilisateur}/supprimer', status_code=200)
def delete_user_by_name(utilisateur: Annotated[str, Path(..., description="User name")]):
    """Delete user by name"""
    return delete_user(database, utilisateur)

@app.put('/utilisateurs/{utilisateur_id}/emprunter/{livre_id}', status_code=200)
def emprunter_livre(utilisateur_id: Annotated[int, Path(..., description="User id")],
                    livre_id: Annotated[int, Path(..., description="Book id")]):
    """Borrow a book"""
    with sqlite3.connect(database) as conn:
        cur = conn.cursor()
        cur.execute("UPDATE livres SET emprunteur_id = ? WHERE id = ?", (utilisateur_id, livre_id))
        return {"message": f"L'utilisateur {utilisateur_id} a emprunté le livre {livre_id}"}
    
@app.put('/utilisateurs/{utilisateur_id}/rendre/{livre_id}', status_code=200)
def rendre_livre(utilisateur_id: Annotated[int, Path(..., description="User id")],
                 livre_id: Annotated[int, Path(..., description="Book id")]):
    """Give back a book"""
    with sqlite3.connect(database) as conn:
        cur = conn.cursor()
        cur.execute("UPDATE livres SET emprunteur_id = NULL WHERE id = ?", (livre_id,))
        return {"message": f"L'utilisateur {utilisateur_id} a rendu le livre {livre_id}"}

uvicorn.run(app, host="0.0.0.0", port=5001)