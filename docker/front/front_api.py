from flask import Flask, render_template, request, session, abort, jsonify
import os
import requests

app = Flask(__name__)

api_service_url = os.environ.get("API_SERVICE_URL", "http://book_api:5001")
URL_EXO = api_service_url + "/"
api_service_auth = os.environ.get("API_SERVICE_AUTH_URL", "http://auth:5005")
URL_EXO_2 = api_service_auth + "/"

app.secret_key = "secret_key_507"

@app.route('/', endpoint='page_accueil')
def index():
    if not 'token' in session :
        return render_template("login.j2")
    else :
        return render_template("accueil.j2")

@app.route('/login_page', endpoint='page_login')
def login_page():
    return render_template("login.j2")

@app.route('/login', endpoint='traitement_login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form.get("username")
        password = request.form.get("password")
        credentials = {"username": username, "password": password}
        response = requests.post(URL_EXO_2 + "token", json=credentials)
        token = response.json().get("access_token")
        if response.status_code == 401:
            return render_template("error.j2", message="Veuillez vous connecter avec des identifiants valides.", login_page=True)
        elif response.status_code == 403:
            return render_template("error.j2", message="Le token a expiré. Veuillez vous reconnecter pour continuer.", login_page=True)
        session['token'] = token
        return render_template("accueil.j2", message="Authentification réussie !")

@app.route('/livres', endpoint='page_livres')
def lettres():
    token = session.get('token')
    headers = {"Authorization": f"Bearer {token}"}
    response = requests.get(URL_EXO + "livres", headers=headers)
    if response.status_code == 401 or response.status_code == 400:
        return render_template("error.j2", message="Veuillez vous connecter avec des identifiants valides.", login_page=True)
    elif response.status_code == 403:
        return render_template("error.j2", message="Le token a expiré. Veuillez vous reconnecter pour continuer.", login_page=True)
    livres = response.json()

    response = requests.get(URL_EXO + "auteurs", headers=headers)
    if response.status_code == 401 or response.status_code == 400:
        return render_template("error.j2", message="Veuillez vous connecter avec des identifiants valides.", login_page=True)
    elif response.status_code == 403:
        return render_template("error.j2", message="Le token a expiré. Veuillez vous reconnecter pour continuer.", login_page=True)
    auteurs = response.json()
    
    map_auteurs = {auteur['id']: auteur['nom_auteur'] for auteur in auteurs}

    for livre in livres:
        livre['auteur_id'] = map_auteurs[livre['auteur_id']]

    h2_title = "Liste des livres"

    return render_template("livres.j2", livres=livres, h2_title=h2_title)

@app.route('/emprunts', endpoint='page_emprunts')
def emprunts():
    token = session.get('token')
    headers = {"Authorization": f"Bearer {token}"}
    response = requests.get(URL_EXO + "livres", headers=headers)
    if response.status_code == 401 or response.status_code == 400:
        return render_template("error.j2", message="Veuillez vous connecter avec des identifiants valides.", login_page=True)
    elif response.status_code == 403:
        return render_template("error.j2", message="Le token a expiré. Veuillez vous reconnecter pour continuer.", login_page=True)
    livres = response.json()

    response = requests.get(URL_EXO + "utilisateurs", headers=headers)
    if response.status_code == 401 or response.status_code == 400:
        return render_template("error.j2", message="Veuillez vous connecter avec des identifiants valides.", login_page=True)
    elif response.status_code == 403:
        return render_template("error.j2", message="Le token a expiré. Veuillez vous reconnecter pour continuer.", login_page=True)
    utilisateurs = {utilisateur['user_id']: utilisateur['username'] for utilisateur in response.json()}

    for livre in livres:
        if livre['emprunteur_id'] is None:
            livre['emprunteur_id'] = "Disponible"
        else:
            livre['emprunteur_id'] = utilisateurs[livre['emprunteur_id']]

    return render_template("emprunts.j2", livres=livres)

@app.route('/result/user', endpoint='page_resultats_user', methods=['POST'])
def get_user():
    token = session.get('token')
    headers = {"Authorization": f"Bearer {token}"}
    username_or_id = request.form['user']
    h2_title = "Résultats de la requête pour : " + username_or_id
    if username_or_id == "":
        response = requests.get(URL_EXO + "utilisateurs", headers=headers)
        if response.status_code == 401 or response.status_code == 400:
            return render_template("error.j2", message="Veuillez vous connecter avec des identifiants valides.", login_page=True)
        elif response.status_code == 403:
            return render_template("error.j2", message="Le token a expiré. Veuillez vous reconnecter pour continuer.", login_page=True)
        h2_title = "Résultats de la requête pour : tous les utilisateurs"
    else:
        response = requests.get(URL_EXO + "utilisateur/" + username_or_id, headers=headers)
        if response.status_code == 401 or response.status_code == 400:
            return render_template("error.j2", message="Veuillez vous connecter avec des identifiants valides.", login_page=True)
        elif response.status_code == 403:
            return render_template("error.j2", message="Le token a expiré. Veuillez vous reconnecter pour continuer.", login_page=True)
        elif response.status_code != 200:
            return render_template("error.j2", message="Utilisateur non trouvé")
    users = response.json()
    emprunts = []
    for user in users:
        user_id = user['user_id']
        response = requests.get(URL_EXO + "utilisateur/emprunts/" + str(user_id), headers=headers)
        if response.status_code == 401 or response.status_code == 400:
            return render_template("error.j2", message="Veuillez vous connecter avec des identifiants valides.", login_page=True)
        elif response.status_code == 403:
            return render_template("error.j2", message="Le token a expiré. Veuillez vous reconnecter pour continuer.", login_page=True)
        user_emprunts = response.json()
        emprunts.extend(user_emprunts)
    response = requests.get(URL_EXO + "auteurs", headers=headers)
    if response.status_code == 401 or response.status_code == 400:
            return render_template("error.j2", message="Veuillez vous connecter avec des identifiants valides", login_page=True)
    elif response.status_code == 403:
            return render_template("error.j2", message="Le token a expiré. Veuillez vous reconnecter pour continuer.", login_page=True)
    auteurs = response.json()
    map_auteurs = {auteur['id']: auteur['nom_auteur'] for auteur in auteurs}

    for emprunt in emprunts:
        emprunt['auteur_id'] = map_auteurs[emprunt['auteur_id']]

    return render_template("resultats.j2", users=users, emprunts=emprunts, h2_title=h2_title)

@app.route('/result/book', endpoint='page_resultats_book', methods=['POST'])
def get_book():
    token = session.get('token')
    headers = {"Authorization": f"Bearer {token}"}
    bookname_or_id = request.form['book']
    h2_title = "Résultats de la requête pour : " + bookname_or_id
    if bookname_or_id == "":
        response = requests.get(URL_EXO + "livres", headers=headers)
        if response.status_code == 401 or response.status_code == 400:
            return render_template("error.j2", message="Veuillez vous connecter avec des identifiants valides.", login_page=True)
        elif response.status_code == 403:
            return render_template("error.j2", message="Le token a expiré. Veuillez vous reconnecter pour continuer.", login_page=True)
        h2_title = "Résultats de la requête pour : tous les livres"
    else:
        response = requests.get(URL_EXO + "livre/" + bookname_or_id, headers=headers)
        if response.status_code == 401 or response.status_code == 400:
            return render_template("error.j2", message="Veuillez vous connecter avec des identifiants valides.", login_page=True)
        elif response.status_code == 403:
            return render_template("error.j2", message="Le token a expiré. Veuillez vous reconnecter pour continuer.", login_page=True)
        elif response.status_code != 200:
            return render_template("error.j2", message="Livre non trouvé")
    livres = response.json()
    response = requests.get(URL_EXO + "auteurs", headers=headers)
    if response.status_code == 401 or response.status_code == 400:
            return render_template("error.j2", message="Veuillez vous connecter avec des identifiants valides.", login_page=True)
    elif response.status_code == 403:
            return render_template("error.j2", message="Le token a expiré. Veuillez vous reconnecter pour continuer.", login_page=True)
    auteurs = response.json()
    map_auteurs = {auteur['id']: auteur['nom_auteur'] for auteur in auteurs}

    for livre in livres:
        livre['auteur_id'] = map_auteurs[livre['auteur_id']]

    return render_template("livres.j2", livres=livres, h2_title=h2_title)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5007)
