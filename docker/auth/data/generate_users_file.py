import bcrypt
import json

# Liste des utilisateurs avec leurs mots de passe
users = [
    {"login": "Alice", "password": "pass1"}, 
    {"login": "John", "password": "john254"}, 
    {"login": "Jane", "password": "123456789"}, 
    {"login": "Clément", "password": "clem_du_92"}, 
    {"login": "Sarah", "password": "srhdb"}, 
    {"login": "Matéo", "password": "matlebg"}, 
    {"login": "Alexis", "password": "aleddd"}, 
    {"login": "Cyprien", "password": "password"}, 
    {"login": "Zlatan", "password": "zlatan"}, 
    {"login": "Florian", "password": "85241"}, 
    {"login": "Nicolas", "password": "1&jbeao89"}
]

# Hasher les mots de passe
for user in users:
    hashed_password = bcrypt.hashpw(user["password"].encode('utf-8'), bcrypt.gensalt())
    user["password"] = hashed_password.decode('utf-8')

# Écrire les utilisateurs dans un fichier JSON
with open("users.json", "w") as f:
    json.dump(users, f)