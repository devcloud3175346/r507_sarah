fastapi==0.104.1
pydantic==2.5.0
bcrypt==3.2.0
python_jose==3.3.0
uvicorn==0.24.0.post1
python-multipart
pytz==2021.1