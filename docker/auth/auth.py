import bcrypt
import fastapi 
from fastapi import FastAPI
from fastapi import HTTPException, Depends, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from datetime import datetime, timedelta
import json
from pydantic import BaseModel
import uvicorn
import pytz

app = FastAPI()

users_passwords = "data/users.json"

# Secret key for JWT
SECRET_KEY = "secret_key_507"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 10

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

def verify_password(plain_password, hashed_password):
    return bcrypt.checkpw(plain_password.encode('utf-8'), hashed_password.encode('utf-8'))

def get_password_hash(password):
    return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')

def authenticate_user(users_passwords, username: str, password: str):
    with open(users_passwords) as f:
        users = json.load(f)
    for user in users:
        print(user)
        if user["login"] == username and verify_password(password, user["password"]):
            return True
    return False

def create_access_token(data: dict, expires_delta: timedelta = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)):
    to_encode = data.copy()
    expire = datetime.now(pytz.timezone('Europe/Paris')) + expires_delta
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

def verify_token(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        if 'exp' in payload:
            expire = datetime.fromtimestamp(payload['exp'])
            if datetime.utcnow() > expire:
                raise credentials_exception
        return username
    except JWTError:
        raise credentials_exception

class Token(BaseModel):
    access_token: str
    token_type: str

class User(BaseModel):
    username: str

class Login(BaseModel):
    username: str
    password: str

@app.post("/token", response_model=Token)
def login_for_access_token(json: Login):
    user = authenticate_user(users_passwords, json.username, json.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": json.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}

@app.get("/users/me/", response_model=User)
def read_users_me(current_user: str = Depends(verify_token)):
    return current_user

uvicorn.run(app, host="0.0.0.0", port=5005)